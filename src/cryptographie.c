#include "cryptographie.h"
#include <stdio.h>

char Chiffrement(char c, int cle) {
    
    if(c < 'A' || c > 'Z') {
        return c;
    }
    else {
        if(c + cle <= 'Z') {
            return c + cle;
        }
        else {
            return 'A' + (((c + cle) - 'Z') - 1);
        }
    }
}

char Dechiffrement(char c, int cle) {

    if(c < 'A' || c > 'Z') {
        return c;
    }
    else {
        if(c - cle >= 'A') {
            return c - cle;
        }
        else {
            return 'Z' + (((c - cle) - 'A') + 1);
        }
    }
}

void AnalyseFreq(char* pszTexte,float jFreq [26]) {
    int max = 0;
    
    for(int i = 0; pszTexte[i] != '\0'; i++) {
        if(pszTexte[i] >= 'A' && pszTexte[i] <= 'Z') {
            jFreq[pszTexte[i] - 'A'] += 1;
            max += 1;
        }
    }
    
}

int CalculCle(float jFreq[26]) {
    float max = 0.0;
    int position_e = 4;
    int position_c = 0;

    for(int i = 0; i < 26; i++) {
        if(jFreq[i] > max) {
            max = jFreq[i];
            position_c = i;
        }
    }

    return position_c - position_e;
}