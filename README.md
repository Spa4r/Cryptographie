# [TITRE DU PROJET]

## Description
[Description du projet]

## Pour commencer
Ces instructions vous permettront d'obtenir une copie du projet sur votre ordinateur.

### Prérequis
Les logiciels à installer sont :
* [Exemple]
```console
foo@bar:~$ [Exemple]
```

### Installation
Une série d'exemples qui vous expliquent, étape par étape, comment faire fonctionner le projet :
* [Exemple]
```console
foo@bar:~$ [Exemple]
```

## Remerciements
* **[Exemple]** - [Exemple](Exemple)

## Auteurs
* **JETON Alex** - *Initial work* - [GitLab](https://gitlab.com/Spaar)

## License
Ce projet est sous licence MIT - voir le fichier [LICENSE](LICENSE) pour plus de détails.